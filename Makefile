# Go parameters

# https://sohlich.github.io/post/go_makefile/

GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=transform-filemaker-json-to-django-models

all: build
build: 
	$(GOBUILD) -o $(BINARY_NAME) -v
	
install: build
	sudo cp transform-filemaker-json-to-django-models /usr/local/bin
	
uninstall:
	sudo rm /usr/local/bin/transform-filemaker-json-to-django-models
	