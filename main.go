package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"bd2l.com.au/csmu/filemaker"
	"bd2l.com.au/csmu/utilities"
	"github.com/joho/godotenv"
)

func stringInSlice(a string, list []string) bool {
	var b string
	for _, b = range list {
		if b == a {
			return true
		}
	}
	return false
}

func main() {

	var err error
	var filemakerFieldsForTablesDirectory *string
	var djangoModelsDirectoryPath *string
	var errorMessages []string
	var errorMessage string
	var filterTablesText *string
	var filterTableSlice []string
	var filterTable string
	var table filemaker.Table
	var fileInfoSlice []os.FileInfo
	var fileInfo os.FileInfo
	var path string
	var process bool
	var outputPath string
	var outputFile *os.File
	var applicationDirectory string
	var environmentPath string
	var style *string

	applicationDirectory, err = filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	environmentPath = filepath.Join(applicationDirectory, ".env")
	if utilities.PathIsFile(environmentPath) {
		if utilities.PathExists(environmentPath) {
			fmt.Println(environmentPath)
			err = godotenv.Load(environmentPath)
		}
	}
	if err != nil {
		panic(err)
	}

	filemakerFieldsForTablesDirectory = flag.String("filemaker-fields-for-tables-directory", "", "The path to the Filemaker JSON table definition directory")
	djangoModelsDirectoryPath = flag.String("django-models-directory-path", "", "The path to the Django models directory")
	filterTablesText = flag.String("filter-tables", "", "A JSON list of tables to work with. Default is no filters and process the lot.")
	style = flag.String("style", filemaker.POSTGRES, "postgres or filemaker")

	flag.Parse()

	fmt.Printf("filemaker-fields-for-tables-directory: %s\n", *filemakerFieldsForTablesDirectory)
	fmt.Printf("django-models-directory-path: %s\n", *djangoModelsDirectoryPath)
	fmt.Printf("Filter tables: %s\n", *filterTablesText)

	if *filemakerFieldsForTablesDirectory == "" {
		*filemakerFieldsForTablesDirectory = utilities.GetEnvironmentVariable("FILEMAKER_FIELDS_FOR_TABLES_DIRECTORY")
		if *filemakerFieldsForTablesDirectory == "" {
			errorMessages = append(errorMessages, "Please supply a valid path to the Filemaker JSON table definition directory")
		}
	}

	if *djangoModelsDirectoryPath == "" {
		*djangoModelsDirectoryPath = utilities.GetEnvironmentVariable("DJANGO_MODELS_DIRECTORY_PATH")
		if *djangoModelsDirectoryPath == "" {
			errorMessages = append(errorMessages, "Please supply a valid path to the Django models directory")
		}
	}

	if *filterTablesText == "" {
		*filterTablesText = utilities.GetEnvironmentVariable("FILTER_TABLES")
	}

	if *filterTablesText != "" {
		err = json.Unmarshal([]byte(*filterTablesText), &filterTableSlice)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			for _, filterTable = range filterTableSlice {
				fmt.Printf("filterTable: %s\n", filterTable)
			}
		}
	}

	if len(errorMessages) > 0 {
		for _, errorMessage = range errorMessages {
			fmt.Println(errorMessage)
		}
	} else {
		fileInfoSlice, err = ioutil.ReadDir(*filemakerFieldsForTablesDirectory)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			for _, fileInfo = range fileInfoSlice {
				process = false
				if len(filterTableSlice) == 0 {
					process = true
				} else {
					process = stringInSlice(fileInfo.Name(), filterTableSlice)
				}

				if process == true {
					path = filepath.Join(*filemakerFieldsForTablesDirectory, fileInfo.Name())
					if utilities.PathIsDirectory(path) {
						table, err = filemaker.GetTableDefinitionFromPath(path)
						if err != nil {
							fmt.Println(err.Error())
						} else {
							outputPath = filepath.Join(*djangoModelsDirectoryPath, table.Base())
							fmt.Println(outputPath)
							outputFile, err = os.Create(outputPath)
							if err != nil {
								fmt.Println(err.Error())
							} else {
								defer outputFile.Close()
								outputFile.Write(table.AsDjangoClass(*style))
								outputFile.Sync()
							}
						}
					}
				}
			}
		}
	}
}
